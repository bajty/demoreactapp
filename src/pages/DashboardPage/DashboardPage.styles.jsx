import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  row: {
    display: "flex",
    justifyContent: "space-between"
  }
}));
