import React from "react";
import { Typography } from "@material-ui/core";
import AsteroidsTable from "../../components/AsteroidsTable/AsteroidsTable.component";
import DateFilter from "../../components/DateFilter/DateFilter.component";
import { useStyles } from "./DashboardPage.styles";

const DashboardPage = () => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <div className={classes.row}>
        <Typography variant="h3">Near Earth Objects</Typography>
        <DateFilter />
      </div>
      <AsteroidsTable />
    </React.Fragment>
  );
};

export default DashboardPage;
