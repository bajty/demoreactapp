import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  heading: {
    marginBottom: theme.spacing(1)
  },
  subheading: {
    marginBottom: theme.spacing(3)
  }
}));
