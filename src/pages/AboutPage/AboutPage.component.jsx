import React from "react";
import { Typography } from "@material-ui/core";
import { useStyles } from "./AboutPage.styles";

const AboutPage = () => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Typography className={classes.heading} variant="h3">
        About React Demo App
      </Typography>
      <Typography className={classes.subheading} variant="h5">
        Neo App
      </Typography>
      <Typography variant="body1">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. In hendrerit gravida
        rutrum quisque non tellus orci ac auctor. Viverra aliquet eget sit amet
        tellus cras adipiscing enim eu. Pharetra pharetra massa massa ultricies
        mi quis hendrerit dolor magna. Eget arcu dictum varius duis at
        consectetur lorem donec massa. Ante in nibh mauris cursus mattis
        molestie. Malesuada fames ac turpis egestas integer. Fermentum iaculis
        eu non diam. Nunc scelerisque viverra mauris in aliquam sem. Sit amet
        mattis vulputate enim nulla aliquet. Non enim praesent elementum
        facilisis leo. Gravida quis blandit turpis cursus in hac habitasse
        platea. Purus gravida quis blandit turpis.
      </Typography>
    </React.Fragment>
  );
};

export default AboutPage;
