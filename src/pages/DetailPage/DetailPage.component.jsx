import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { fetchAsteroidDetail } from "../../redux/asteroid-detail/asteroid-detail.actions";
import {
  selectAsteroidDetail,
  selectAsteroidDetailIsLoading
} from "../../redux/asteroid-detail/asteroid-detail.selectors";
import AsteroidDetail from "../../components/AsteroidDetail/AsteroidDetail.component";
import AsteroidNotFound from "../../components/AsteroidNotFound/AsteroidNotFound.component";

const DetailPage = () => {
  let { id } = useParams();

  const dispatch = useDispatch();

  const asteroid = useSelector(selectAsteroidDetail);
  const isLoading = useSelector(selectAsteroidDetailIsLoading);

  useEffect(() => {
    dispatch(fetchAsteroidDetail(id));
  }, [dispatch, id]);

  return (
    <React.Fragment>
      {asteroid ? (
        <AsteroidDetail asteroid={asteroid} />
      ) : isLoading ? (
        <div></div>
      ) : (
        <AsteroidNotFound />
      )}
    </React.Fragment>
  );
};

export default DetailPage;
