const API_KEY = "75JG1aaDllOr4ba7HzSTyPQW30QjgFRa9YFer2q2";
const API_URL = "https://api.nasa.gov/neo/rest/v1/";

export const createFetchAsteroidsUrl = ({ from, to }) =>
  `${API_URL}feed?start_date=${from}&end_date=${to}&api_key=${API_KEY}`;

export const createFetchAsteroidDetailUrl = asteroidID =>
  `${API_URL}neo/${asteroidID}?api_key=${API_KEY}`;

export const createAsteroidTableRowObject = (
  id,
  name,
  closeApproachDate,
  estimatedDiameter,
  missDistance,
  potentiallyHazardous
) => {
  return {
    id,
    name,
    closeApproachDate,
    estimatedDiameter,
    missDistance,
    potentiallyHazardous
  };
};

export const createAsteroidDetailObject = (
  id,
  name,
  estimatedDiameterMin,
  estimatedDiameterMax,
  nasaJplUrl,
  potentiallyHazardous,
  closeApproaches
) => {
  return {
    id,
    name,
    estimatedDiameterMin,
    estimatedDiameterMax,
    nasaJplUrl,
    potentiallyHazardous,
    closeApproaches
  };
};
