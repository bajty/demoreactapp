import AsteroidDetailActionTypes from "./asteroid-detail.types";

const INITIAL_STATE = {
  asteroidDetail: null,
  isFetching: false,
  errorMessage: undefined
};

const asteroidDetailReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AsteroidDetailActionTypes.FETCH_ASTEROIDDETAIL_START:
      return {
        ...state,
        isFetching: true,
        asteroidDetail: null,
        errorMessage: undefined
      };
    case AsteroidDetailActionTypes.FETCH_ASTEROIDDETAIL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: undefined,
        asteroidDetail: action.payload
      };
    case AsteroidDetailActionTypes.FETCH_ASTEROIDDETAIL_FAILURE:
      return {
        ...state,
        isFetching: false,
        asteroidDetail: null,
        errorMessage: action.payload
      };
    default:
      return state;
  }
};

export default asteroidDetailReducer;
