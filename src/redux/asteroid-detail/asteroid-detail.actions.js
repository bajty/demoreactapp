import AsteroidDetailActionTypes from "./asteroid-detail.types";
import { createFetchAsteroidDetailUrl } from "../../utils/nasa-neo-api";

export const fetchAsteroidDetailStart = () => ({
  type: AsteroidDetailActionTypes.FETCH_ASTEROIDDETAIL_START
});

export const fetchAsteroidDetailSuccess = asteroidDetail => ({
  type: AsteroidDetailActionTypes.FETCH_ASTEROIDDETAIL_SUCCESS,
  payload: asteroidDetail
});

export const fetchAsteroidDetailFailure = errorMessage => ({
  type: AsteroidDetailActionTypes.FETCH_ASTEROIDDETAIL_FAILURE,
  payload: errorMessage
});

export const fetchAsteroidDetail = asteroidID => {
  return dispatch => {
    dispatch(fetchAsteroidDetailStart());
    fetch(createFetchAsteroidDetailUrl(asteroidID))
      .then(res => {
        if (!res.ok) {
          throw Error(res.status);
        }
        return res;
      })
      .then(res => res.json())
      .then(data => {
        dispatch(fetchAsteroidDetailSuccess(data));
      })
      .catch(err => {
        dispatch(fetchAsteroidDetailFailure(err.message));
      });
  };
};
