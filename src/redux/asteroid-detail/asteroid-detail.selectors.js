import { createSelector } from "reselect";
import { createAsteroidDetailObject } from "../../utils/nasa-neo-api";

const selectAsteroidDetailResponse = state => state.asteroidDetail;

export const selectAsteroidDetailIsLoading = createSelector(
  [selectAsteroidDetailResponse],
  ({ isFetching }) => isFetching
);

export const selectAsteroidDetail = createSelector(
  [selectAsteroidDetailResponse],
  ({ asteroidDetail }) => {
    let asteroid = null;
    let approaches = [];
    if (asteroidDetail) {
      if (asteroidDetail.close_approach_data) {
        approaches = asteroidDetail.close_approach_data.map(data => {
          return {
            date: data.close_approach_date,
            missDistance: data.miss_distance.kilometers,
            relativeVelocity: data.relative_velocity.kilometers_per_hour,
            orbitingBody: data.orbiting_body
          };
        });
      }

      asteroid = createAsteroidDetailObject(
        asteroidDetail.id,
        asteroidDetail.name,
        asteroidDetail.estimated_diameter.meters.estimated_diameter_min.toString(),
        asteroidDetail.estimated_diameter.meters.estimated_diameter_max.toString(),
        asteroidDetail.nasa_jpl_url,
        asteroidDetail.is_potentially_hazardous_asteroid,
        approaches
      );
    }

    return asteroid;
  }
);
