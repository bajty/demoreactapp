import { combineReducers } from "redux";
import asteroidsReducer from "./asteroids/asteroids.reducer";
import asteroidDetailReducer from "./asteroid-detail/asteroid-detail.reducer";

const rootReducer = combineReducers({
  asteroids: asteroidsReducer,
  asteroidDetail: asteroidDetailReducer
});

export default rootReducer;
