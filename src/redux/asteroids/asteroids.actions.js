import AsteroidsActionTypes from "./asteroids.types";
import { createFetchAsteroidsUrl } from "../../utils/nasa-neo-api";

export const fetchAsteroidsStart = () => ({
  type: AsteroidsActionTypes.FETCH_ASTEROIDS_START
});

export const fetchAsteroidsSuccess = asteroids => ({
  type: AsteroidsActionTypes.FETCH_ASTEROIDS_SUCCESS,
  payload: asteroids
});

export const fetchAsteroidsFailure = errorMessage => ({
  type: AsteroidsActionTypes.FETCH_ASTEROIDS_FAILURE,
  payload: errorMessage
});

export const fetchAsteroids = ({ from, to }) => {
  return dispatch => {
    dispatch(fetchAsteroidsStart());
    fetch(createFetchAsteroidsUrl({ from, to }))
      .then(res => {
        if (!res.ok) {
          throw Error(res.status);
        }
        return res;
      })
      .then(res => res.json())
      .then(data => {
        dispatch(fetchAsteroidsSuccess(data));
      })
      .catch(err => {
        dispatch(fetchAsteroidsFailure(err.message));
      });
  };
};
