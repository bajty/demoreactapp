import AsteroidsActionTypes from "./asteroids.types";

const INITIAL_STATE = {
  asteroids: null,
  isFetching: false,
  errorMessage: undefined
};

const asteroidsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AsteroidsActionTypes.FETCH_ASTEROIDS_START:
      return {
        ...state,
        isFetching: true
      };
    case AsteroidsActionTypes.FETCH_ASTEROIDS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: undefined,
        asteroids: action.payload
      };
    case AsteroidsActionTypes.FETCH_ASTEROIDS_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload
      };
    default:
      return state;
  }
};

export default asteroidsReducer;
