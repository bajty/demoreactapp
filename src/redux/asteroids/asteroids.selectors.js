import { createSelector } from "reselect";
import { createAsteroidTableRowObject } from "../../utils/nasa-neo-api";

const selectAsteroidsResponse = state => state.asteroids;

export const selectAsteroidsIsLoading = createSelector(
  [selectAsteroidsResponse],
  ({ isFetching }) => isFetching
);

export const selectAsteroids = createSelector(
  [selectAsteroidsResponse],
  response => {
    let data = [];
    if (response.asteroids && response.asteroids.near_earth_objects) {
      Object.keys(response.asteroids.near_earth_objects).forEach(date => {
        response.asteroids.near_earth_objects[date].forEach(asteroid => {
          data.push(
            createAsteroidTableRowObject(
              asteroid.id,
              asteroid.name,
              asteroid.close_approach_data[0].close_approach_date,
              asteroid.estimated_diameter.meters.estimated_diameter_min.toString(),
              asteroid.close_approach_data[0].miss_distance.kilometers,
              asteroid.is_potentially_hazardous_asteroid
            )
          );
        });
      });
    }
    data.sort((a, b) =>
      a.closeApproachDate > b.closeApproachDate
        ? 1
        : b.closeApproachDate > a.closeApproachDate
        ? -1
        : 0
    );
    return data;
  }
);
