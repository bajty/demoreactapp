import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  logo: {
    display: "inline-block",
    color: theme.palette.grey[900],
    width: "2rem",
    marginRight: "0.5rem"
  },
  logoContainer: {
    display: "inline-block",
    width: 150,
    cursor: "pointer"
  },
  logoText: {
    display: "inline-block"
  }
}));
