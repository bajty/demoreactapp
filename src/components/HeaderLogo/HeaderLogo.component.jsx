import React from "react";
import { Typography, Box } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { ReactComponent as Logo } from "../../assets/logo.svg";
import { useStyles } from "./HeaderLogo.styles";

const HeaderLogo = () => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <Box
      className={classes.logoContainer}
      onClick={() => {
        history.push("/");
      }}
    >
      <Logo className={classes.logo} />
      <Typography className={classes.logoText} variant="h4">
        Neo
      </Typography>
    </Box>
  );
};

export default HeaderLogo;
