import React from "react";
import { Toolbar, AppBar, IconButton, Tooltip } from "@material-ui/core";
import { InfoOutlined as InfoIcon } from "@material-ui/icons";
import { Link } from "react-router-dom";
import { useStyles } from "./Header.styles";
import HeaderSearch from "../HeaderSearch/HeaderSearch.component";
import HeaderLogo from "../HeaderLogo/HeaderLogo.component";

const Header = () => {
  const classes = useStyles();

  return (
    <AppBar className={classes.mainHeader}>
      <Toolbar>
        <HeaderLogo />
        <HeaderSearch />
        <Link to="/about">
          <Tooltip title="About">
            <IconButton>
              <InfoIcon />
            </IconButton>
          </Tooltip>
        </Link>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
