import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  mainHeader: {
    background: "#fafafa",
    color: theme.palette.grey[900]
  }
}));
