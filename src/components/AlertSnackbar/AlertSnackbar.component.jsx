import React, { useState, useEffect } from "react";
import MuiAlert from "@material-ui/lab/Alert";
import { Snackbar } from "@material-ui/core";

const AlertSnackbar = ({ errorMessage }) => {
  const [err, setErr] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setErr(false);
  };

  useEffect(() => {
    !!errorMessage && setErr(true);
  }, [errorMessage]);

  return (
    <Snackbar open={err} autoHideDuration={3000} onClose={handleClose}>
      <MuiAlert
        elevation={6}
        variant="filled"
        severity="error"
        onClose={handleClose}
      >
        {errorMessage}
      </MuiAlert>
    </Snackbar>
  );
};

export default AlertSnackbar;
