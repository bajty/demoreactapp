import React from "react";
import { useStyles } from "./AsteroidDetail.styles";
import { Paper, Typography, Grid, Button } from "@material-ui/core";
import CloseApproachesTable from "../CloseApproachesTable/CloseApproachesTable.component";

const AsteroidDetail = ({ asteroid }) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Paper className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Typography className={classes.row} variant="h5">
              SPK-ID: {asteroid.id}
            </Typography>
            <Typography className={classes.row} variant="h5">
              Name: {asteroid.name}
            </Typography>
            <Button
              target="_blank"
              href={asteroid.nasaJplUrl}
              variant="outlined"
              size="large"
            >
              NASA jpl link
            </Button>
          </Grid>
          <Grid item xs={8}>
            <Typography className={classes.row} variant="h5">
              Potentially hazardous:{" "}
              {asteroid.potentiallyHazardous ? "YES" : "NO"}
            </Typography>
            <Typography className={classes.row} variant="h5">
              Minimal estimated diameter: {asteroid.estimatedDiameterMin} m
            </Typography>
            <Typography className={classes.row} variant="h5">
              Maximal estimated diameter: {asteroid.estimatedDiameterMax} m
            </Typography>
          </Grid>
        </Grid>
      </Paper>
      <CloseApproachesTable closeApproaches={asteroid.closeApproaches} />
    </React.Fragment>
  );
};

export default AsteroidDetail;
