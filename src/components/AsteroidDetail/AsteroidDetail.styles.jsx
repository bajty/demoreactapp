import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[100],
    marginBottom: theme.spacing(3)
  },
  row: {
    marginBottom: theme.spacing(2)
  }
}));
