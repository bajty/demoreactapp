import React from "react";
import {
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";
import { columns } from "./CloseApproachesTable.utils";
import { useStyles } from "./CloseApproachesTable.styles";

const CloseApproachesTable = ({ closeApproaches }) => {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <TableContainer>
        <Table stickyHeader size="small">
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {closeApproaches.map(approach => (
              <TableRow
                hover
                tabIndex={-1}
                key={`${approach.date}-${Math.floor(Math.random() * 100)}`}
              >
                {columns.map(column => {
                  const value = approach[column.id];
                  return (
                    <TableCell key={column.id} align={column.align}>
                      {column.format ? column.format(value) : value}
                    </TableCell>
                  );
                })}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export default CloseApproachesTable;
