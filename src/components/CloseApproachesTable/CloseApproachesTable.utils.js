export const columns = [
  {
    id: "date",
    label: "Date",
    minWidth: 100,
    align: "left"
  },
  {
    id: "missDistance",
    label: "Miss distance (km)",
    minWidth: 170,
    align: "right"
  },
  {
    id: "relativeVelocity",
    label: "Relative velocity (km/h)",
    minWidth: 170,
    align: "right"
  },
  {
    id: "orbitingBody",
    label: "Orbiting body",
    minWidth: 170,
    align: "right"
  }
];
