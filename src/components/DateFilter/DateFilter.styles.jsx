import { makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { grey } from "@material-ui/core/colors";

export const useStyles = makeStyles({
  datePicker: {
    marginLeft: "1rem"
  }
});

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: grey[400]
    }
  }
});
