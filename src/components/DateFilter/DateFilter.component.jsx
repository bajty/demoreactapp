import React, { useState, useEffect } from "react";
import { useStyles, theme } from "./DateFilter.styles";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { useDispatch } from "react-redux";
import { fetchAsteroids } from "../../redux/asteroids/asteroids.actions";
import { ThemeProvider } from "@material-ui/core";
import { dateDiffInDays } from "./DateFilter.utils";
import AlertSnackbar from "../AlertSnackbar/AlertSnackbar.component";

const DateFilter = () => {
  const [from, setFrom] = useState("2020-03-01");
  const [to, setTo] = useState("2020-03-05");
  const [err, setErr] = useState(null);
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    const f = new Date(from);
    const t = new Date(to);
    if (f > t) {
      console.error("Bad Date Request");
      setErr(`${Math.floor(Math.random() * 100)} - Bad Date Request`);
      return;
    }
    const diff = dateDiffInDays(f, t);
    if (diff <= 7) {
      dispatch(fetchAsteroids({ from, to }));
    } else {
      console.error("Max 7 days - diff:", diff);
      setErr(`${Math.floor(Math.random() * 100)} - Max 7 days`);
    }
  }, [from, to, dispatch]);

  function isValidDate(date) {
    return date instanceof Date && !isNaN(date);
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <ThemeProvider theme={theme}>
        <div>
          <KeyboardDatePicker
            className={classes.datePicker}
            autoOk
            disableToolbar
            variant="inline"
            format="yyyy-MM-dd"
            margin="normal"
            id="date-picker-from"
            label="From Date"
            value={from}
            onChange={date => {
              isValidDate(date) && setFrom(date.toISOString().substring(0, 10));
            }}
          />
          <KeyboardDatePicker
            className={classes.datePicker}
            autoOk
            disableToolbar
            variant="inline"
            format="yyyy-MM-dd"
            margin="normal"
            id="date-picker-to"
            label="To Date"
            value={to}
            onChange={date => {
              isValidDate(date) && setTo(date.toISOString().substring(0, 10));
            }}
          />
        </div>
      </ThemeProvider>
      <AlertSnackbar errorMessage={err} />
    </MuiPickersUtilsProvider>
  );
};

export default DateFilter;
