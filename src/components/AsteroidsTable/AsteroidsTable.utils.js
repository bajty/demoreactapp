export const columns = [
  {
    id: "id",
    label: "SPK-ID",
    minWidth: 100,
    align: "left"
  },
  {
    id: "name",
    label: "Name",
    minWidth: 170,
    align: "left"
  },
  {
    id: "closeApproachDate",
    label: "Close approach date",
    minWidth: 170,
    align: "right"
  },
  {
    id: "estimatedDiameter",
    label: "Estimated diameter (m)",
    minWidth: 170,
    align: "right"
  },
  {
    id: "missDistance",
    label: "Miss distance (km)",
    minWidth: 170,
    align: "right"
  },
  {
    id: "potentiallyHazardous",
    label: "Potentially hazardous",
    minWidth: 170,
    align: "right",
    format: value => (value ? "YES" : "NO")
  }
];
