import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles({
  container: {
    maxHeight: 440
  },
  row: {
    cursor: "pointer"
  }
});
