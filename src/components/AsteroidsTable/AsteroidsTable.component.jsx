import React, { useState } from "react";
import { useSelector } from "react-redux";
import { columns } from "./AsteroidsTable.utils";
import { useHistory } from "react-router-dom";
import {
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination
} from "@material-ui/core";
import { selectAsteroids } from "../../redux/asteroids/asteroids.selectors";
import { useStyles } from "./AsteroidsTable.styles";

const AsteroidsTable = () => {
  const Asteroids = useSelector(selectAsteroids);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const history = useHistory();
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper>
      <TableContainer className={classes.container}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {Asteroids.slice(
              page * rowsPerPage,
              page * rowsPerPage + rowsPerPage
            ).map(asteroid => (
              <TableRow
                className={classes.row}
                hover
                tabIndex={-1}
                key={asteroid.id}
                onClick={() => {
                  history.push(`/asteroid/${asteroid.id}`);
                }}
              >
                {columns.map(column => {
                  const value = asteroid[column.id];
                  return (
                    <TableCell key={column.id} align={column.align}>
                      {column.format ? column.format(value) : value}
                    </TableCell>
                  );
                })}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={Asteroids.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default AsteroidsTable;
