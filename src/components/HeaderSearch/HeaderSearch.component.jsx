import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { InputBase, IconButton, Tooltip } from "@material-ui/core";
import { Search as SearchIcon } from "@material-ui/icons";
import { useStyles } from "./HeaderSearch.styles";
import AlertSnackbar from "../AlertSnackbar/AlertSnackbar.component";

const Search = () => {
  const classes = useStyles();
  const [search, setSearch] = useState("");
  const [err, setErr] = useState(null);
  const history = useHistory();

  const onSearch = event => {
    event.preventDefault();
    if (/(^[1-3][0-9]{6}$|^5[0-9]{7}$)/i.test(search)) {
      history.push(`/asteroid/${search}`);
    } else {
      console.error("Bad search pattern:", search);
      setErr(
        `${Math.floor(Math.random() * 100)} - Bad search pattern: ${search}`
      );
    }
  };

  return (
    <React.Fragment>
      <form className={classes.searchForm} onSubmit={onSearch}>
        <InputBase
          className={classes.searchInput}
          value={search}
          onChange={event => {
            setSearch(event.target.value);
          }}
          placeholder="Search asteroid (SPK-ID)"
        />
        <Tooltip title="Search">
          <IconButton type="submit">
            <SearchIcon />
          </IconButton>
        </Tooltip>
      </form>
      <AlertSnackbar errorMessage={err} />
    </React.Fragment>
  );
};

export default Search;
