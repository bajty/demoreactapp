import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  searchForm: {
    marginLeft: "auto",
    marginRight: "auto"
  },
  searchInput: {
    marginRight: "0.5rem",
    width: "13vw"
  }
}));
