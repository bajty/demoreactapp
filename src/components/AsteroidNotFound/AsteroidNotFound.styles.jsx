import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  heading: {
    marginTop: theme.spacing(16)
  },
  asteroidFalling: {
    maxWidth: 400
  }
}));
