import React from "react";
import { Typography } from "@material-ui/core";
import { useStyles } from "./AsteroidNotFound.styles";
import { ReactComponent as AsteroidFalling } from "../../assets/asteroid_falling.svg";

const AsteroidNotFound = () => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Typography className={classes.heading} align="center" variant="h3">
        ASTEROID NOT FOUND
      </Typography>
      <AsteroidFalling className={classes.asteroidFalling} />
    </React.Fragment>
  );
};

export default AsteroidNotFound;
