import React from "react";

import { Switch, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import { Box, Container, Backdrop, CircularProgress } from "@material-ui/core";

import { useStyles } from "./App.styles";
import { selectAsteroidsIsLoading } from "./redux/asteroids/asteroids.selectors";
import { selectAsteroidDetailIsLoading } from "./redux/asteroid-detail/asteroid-detail.selectors";
import Header from "./components/Header/Header.component";
import AboutPage from "./pages/AboutPage/AboutPage.component";
import DashboardPage from "./pages/DashboardPage/DashboardPage.component";
import DetailPage from "./pages/DetailPage/DetailPage.component";

function App() {
  const classes = useStyles();
  const asteroidsIsLoading = useSelector(selectAsteroidsIsLoading);
  const asteroidDetailIsLoading = useSelector(selectAsteroidDetailIsLoading);
  return (
    <Box>
      <Header />
      <Container className={classes.root}>
        <Switch>
          <Route path="/about" component={AboutPage} />
          <Route path="/asteroid/:id" component={DetailPage} />
          <Route path="/" component={DashboardPage} />
        </Switch>
        <Backdrop
          className={classes.backdrop}
          open={asteroidDetailIsLoading || asteroidsIsLoading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Container>
    </Box>
  );
}

export default App;
